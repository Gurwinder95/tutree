import React from 'react';

import {View, TextInput, StyleSheet, Text} from 'react-native';

const FormTextInput = ({
  placeholder,
  autoCapitalize,
  onChangeText,
  keyboardType,
  maxLength,
  secureTextEntry,
  blurOnSubmit,
  onSubmitEditing,
  returnKeyType,
  refHandler,
}) => {
  // console.log(refHandler);
  return (
    <View>
      <TextInput
        placeholder={placeholder}
        autoCapitalize={autoCapitalize}
        keyboardType={keyboardType}
        maxLength={maxLength}
        secureTextEntry={secureTextEntry}
        onChangeText={onChangeText}
        blurOnSubmit={blurOnSubmit}
        onSubmitEditing={onSubmitEditing}
        returnKeyType={returnKeyType}
        ref={refHandler}
        style={styles.input}
      />
    </View>
  );
};

export default FormTextInput;

const styles = StyleSheet.create({
  container: {
    paddingTop: 23,
  },
  input: {
    margin: 15,
    height: 40,
    borderColor: '#7a42f4',
    borderWidth: 1,
  },
});

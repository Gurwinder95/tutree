import React from 'react';
import {View} from 'react-native';
import {CheckBox} from 'react-native-elements';
const FormCheckBox = ({title, checked, onIconPress}) => {
  return (
    <View>
      <CheckBox title={title} checked={checked} onIconPress={onIconPress} />
    </View>
  );
};
export default FormCheckBox;

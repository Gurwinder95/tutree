import React from 'react';

import {View} from 'react-native';
import {Dropdown} from 'react-native-material-dropdown';

const FormDropDown = ({label, data, onChangeText, onBlur, refHandler}) => {
  return (
    <View>
      <Dropdown
        label={label}
        data={data}
        onChangeText={onChangeText}
        onBlur={onBlur}
        ref={refHandler}
      />
    </View>
  );
};
export default FormDropDown;

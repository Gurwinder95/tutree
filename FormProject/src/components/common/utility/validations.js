import React from 'react';

import {Alert} from 'react-native';

export const validations = {
  nameErrorHandler,
  ageErrorHandler,
  emailErrorHandler,
  phoneErrorHandler,
  passwordErrorHandler,
  confirmPasswordErrorHander,
};

function nameErrorHandler(name) {
  if (name.length <= 3 || name.charAt(0) == ' ') {
    Alert.alert('Please enter a valid name');
    return 0;
  }
}

function ageErrorHandler(age) {
  if (age.length < 2) {
    alert('Please enter a valid age in the range 10 to 99');
    return 0;
  }
}

function emailErrorHandler(email) {
  const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (!email.match(mailformat)) {
    alert('Please enter a valid email as abc@gmail.com');
    return 0;
  }
}
function phoneErrorHandler(phone) {
  if (phone.length != 10) {
    alert('Enter a valid 10 digit number');
    return 0;
  }
}

function passwordErrorHandler(password) {
  if (password.length < 5) {
    alert('Password should be of minimum 5 characters');
    return 0;
  }
}

function confirmPasswordErrorHander(password, confirmPassword) {
  if (confirmPassword != password) {
    alert('Password not matches');
    return 0;
  }
}

//export default validations;

import React, {Component} from 'react';
import {View, StyleSheet, Text, ScrollView, Button} from 'react-native';

import FormTextInput from './FormTextInput';
import FormDropDown from './FormDropDown';
import FormCheckBox from './FormCheckBox';
import {validations} from '../components/common/utility/validations';

class Inputs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      age: '',
      email: '',
      gender: '',
      genderId: '',
      phone: '',
      password: '',
      confirmPassword: '',
      agree: false,
    };
    this.inputsArray = {};
  }

  focusNextField = id => {
    console.log(id);
    this.inputsArray[id].focus();
  };

  handleName = data => {
    this.setState({name: data});
  };
  handleAge = data => {
    this.setState({age: data});
  };

  handleEmail = data => {
    this.setState({email: data});
  };

  handlePhone = data => {
    this.setState({phone: data});
  };
  handlePassword = data => {
    this.setState({password: data});
  };
  handleConfirmPassword = data => {
    this.setState({confirmPassword: data});
  };

  handleGender = (val, i, data) => {
    //  console.log('Value=' + val + '  id=' + data[ind].id);
    const id = data[i].id;
    this.setState({gender: val, genderId: id});
  };

  // nameValidate = name => {
  //   let nameData = validations.nameErrorHandler(name);
  //   if (nameData == 0) {
  //   }
  // };

  handlerFormData = () => {
    const result = JSON.stringify(this.state);
    const {
      name,
      age,
      email,
      phone,
      password,
      confirmPassword,
      gender,
      agree,
    } = this.state;
    if (
      name == '' ||
      name.length <= 3 ||
      name.charAt(0) == ' ' ||
      age == '' ||
      age.length < 2 ||
      email == '' ||
      !email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/) ||
      phone == '' ||
      phone.length != 10 ||
      password == '' ||
      confirmPassword == '' ||
      password != confirmPassword ||
      gender == '' ||
      agree == false
    ) {
      alert('Please fill all the form fields correctly');
    } else {
      alert('Success!! Proceed to the next step');
    }
  };

  render() {
    const {
      name,
      age,
      phone,
      email,
      password,
      confirmPassword,
      gender,
      genderId,
      agree,
    } = this.state;

    let data = [
      {
        id: 1,
        value: 'Male',
      },
      {
        id: 2,
        value: 'Female',
      },
    ];

    return (
      <View>
        <ScrollView>
          <FormTextInput
            placeholder="Name"
            autoCapitalize="characters"
            keyboardType="default"
            maxLength={50}
            onChangeText={this.handleName}
            blurOnSubmit={false}
            onSubmitEditing={() => {
              let nameData = validations.nameErrorHandler(name);
              if (nameData == 0) {
                return;
              }
              // this.nameValidate(name);
              this.focusNextField('Age');
            }}
            returnKeyType={'next'}
            refHandler={input => {
              this.inputsArray['Name'] = input;
            }}
          />

          <FormTextInput
            placeholder="Age"
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={2}
            onChangeText={this.handleAge}
            blurOnSubmit={false}
            onSubmitEditing={() => {
              let ageData = validations.ageErrorHandler(age);
              if (ageData == 0) {
                return;
              }
              this.focusNextField('Email');
            }}
            returnKeyType={'next'}
            refHandler={input => {
              this.inputsArray['Age'] = input;
            }}
          />
          <FormTextInput
            placeholder="Email"
            autoCapitalize="none"
            keyboardType="email-address"
            maxLength={50}
            onChangeText={this.handleEmail}
            blurOnSubmit={false}
            onSubmitEditing={() => {
              let emailData = validations.emailErrorHandler(email);
              if (emailData == 0) {
                return;
              }

              this.focusNextField('Phone');
            }}
            returnKeyType={'next'}
            refHandler={input => {
              this.inputsArray['Email'] = input;
            }}
          />
          <FormTextInput
            placeholder="Phone"
            autoCapitalize="none"
            keyboardType="numeric"
            maxLength={10}
            onChangeText={this.handlePhone}
            blurOnSubmit={false}
            onSubmitEditing={() => {
              let phoneData = validations.phoneErrorHandler(phone);
              if (phoneData == 0) {
                return;
              }

              this.focusNextField('Password');
            }}
            returnKeyType={'next'}
            refHandler={input => {
              this.inputsArray['Phone'] = input;
            }}
          />
          <FormTextInput
            placeholder="Password"
            autoCapitalize="none"
            keyboardType="default"
            maxLength={20}
            onChangeText={this.handlePassword}
            secureTextEntry={true}
            blurOnSubmit={false}
            onSubmitEditing={() => {
              let passwordData = validations.passwordErrorHandler(password);
              if (passwordData == 0) {
                return;
              }

              this.focusNextField('Confirm Password');
            }}
            returnKeyType={'next'}
            refHandler={input => {
              this.inputsArray['Password'] = input;
            }}
          />
          <FormTextInput
            placeholder="Confirm Password"
            autoCapitalize="none"
            keyboardType="default"
            maxLength={20}
            secureTextEntry={true}
            onChangeText={this.handleConfirmPassword}
            blurOnSubmit={false}
            onSubmitEditing={() => {
              let confirmPasswordData = validations.confirmPasswordErrorHander(
                password,
                confirmPassword,
              );
              if (confirmPasswordData == 0) {
                return;
              }
              this.focusNextField('Gender');
            }}
            returnKeyType={'next'}
            refHandler={input => {
              this.inputsArray['Confirm Password'] = input;
            }}
          />
          <FormDropDown
            label="Gender"
            data={data}
            onChangeText={(value, index, data) =>
              this.handleGender(value, index, data)
            }
            onBlur={false}
            refHandler={input => {
              this.inputsArray['Gender'] = input;
            }}
          />

          <FormCheckBox
            title="I agree"
            checked={agree}
            onIconPress={() => {
              this.setState({agree: !agree});
            }}
          />

          <Button title="Submit" onPress={this.handlerFormData}></Button>

          <Text>Are you agree ? {agree ? 'yes' : 'no'}</Text>
          <Text>Your name is {name}</Text>
          <Text>Your age is {age}</Text>
          <Text>Your email is {email}</Text>
          <Text>
            Your password is {password} and confirmPassword is {confirmPassword}
          </Text>
          <Text>
            You are {gender} having gender id {genderId}
          </Text>
          <Text>Your phone number is {phone}</Text>
        </ScrollView>
      </View>
    );
  }
}
export default Inputs;

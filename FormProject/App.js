import React, {Component} from 'react';
import {Text, View} from 'react-native';
import Inputs from './src/components/Inputs';

export default class App extends Component {
  render() {
    return (
      <View style={{flex: 1, paddingTop: 20}}>
        {/* <Text> Hello</Text> */}
        <Inputs />
      </View>
    );
  }
}
